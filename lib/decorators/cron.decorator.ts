export function Cron(pattern: string) {
  return function (target: any, key: string, descriptor: PropertyDescriptor) {
    const entry = { pattern, value: descriptor.value, name: key };
    
    if (target.cronMethods) {
      target.cronMethods.push(entry);
    } else {
      target.cronMethods = [ entry ];
    }
  }
}
