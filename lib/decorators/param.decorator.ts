import { ParamOptions } from '@cmd-handler/models/param-options.model';
import { CommandModel } from '../command-handler/models/command.model';

export function Param(name: string, help: string, options?: ParamOptions) {
  return function (target: any, key: string, descriptor: PropertyDescriptor) {
    if (target.commands) {
      const commandIndex = target.commands.findIndex((c: CommandModel) => c.key === key);
      
      if (commandIndex !== -1) {
        const command = target.commands[commandIndex];
        command.params.unshift({ name, help, options: options || {} });
      }
    }
  }
}