import { Database } from 'lib/services/database.service';
import { container, Lifecycle, scoped } from 'tsyringe';

export function Repo<T>(entity: new () => T) {
  return function (constructor: any) {
    const database = container.resolve(Database);
    
    database.connection$.subscribe(connection => {
      if (connection) {
        constructor.prototype.repo = database.getManager(entity);
      }
    });

    scoped(Lifecycle.ContainerScoped)(constructor);
  }
}
