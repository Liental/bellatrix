export const getMessageSymbol = Symbol('getMessage');

export function GetMessage() {
  return function (target: any, methodName: string, parameterIndex: number) {
    const metadata = Reflect.getOwnMetadata(getMessageSymbol, target, methodName) || [];

    metadata.push(parameterIndex);
    Reflect.defineMetadata(getMessageSymbol, metadata, target, methodName);  
  }
}
