import { ParamOptions } from '@cmd-handler/models/param-options.model';
import { Param } from './param.decorator';

export function OptionalParam(name: string, help: string, options?: ParamOptions) {
  return function (target: any, key: string, descriptor: PropertyDescriptor) {
    options = Object.assign(options || {}, { optional: true });
    Param(name, help, options)(target, key, descriptor);
  }
}