import { CronJob } from 'cron';
import { setDecoratorValues } from '@cmd-helpers/set-decorator-value.helper';
import { ClientManager } from 'lib/services/client-manager.service';
import { container, Lifecycle, scoped } from 'tsyringe';
import { getClientSymbol } from './client.decorator';

export function Module(prefix: string, help: string) {
  return function (constructor: any) {
    const methods = constructor.prototype.cronMethods || [];
    constructor.prototype.prefix = prefix;
    constructor.prototype.help = help;
    
    scoped(Lifecycle.ContainerScoped)(constructor)
    const instance = container.resolve(constructor);

    const clientCallback = () => {
      const manager = container.resolve(ClientManager);
      return manager.client;
    }

    const propertyNames = Object.getOwnPropertyNames(constructor.prototype);
    const constructorMethods = propertyNames.filter(name => typeof constructor.prototype[name] === 'function');

    for (const methodName of constructorMethods) {
      setDecoratorValues(getClientSymbol, constructor.prototype, methodName, clientCallback, instance);
    }

    for (const method of methods) {
      const cronJob = new CronJob(method.pattern, (...args: any) => {
        setDecoratorValues(getClientSymbol, instance, method.name, clientCallback);
        instance[method.name].bind(instance)(...args);
      }, null, true, null, instance);
      
      cronJob.start();
      (instance as any).logger.log(`Registered a CRON job (${method.name})`);
    }
  };
}
