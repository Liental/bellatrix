export const getClientSymbol = Symbol('getClient');

export function GetClient() {
  return function (target: any, methodName: string, parameterIndex: number) {
    const metadata = Reflect.getOwnMetadata(getClientSymbol, target, methodName) || [];

    metadata.push(parameterIndex);
    Reflect.defineMetadata(getClientSymbol, metadata, target, methodName);  
  }
}
