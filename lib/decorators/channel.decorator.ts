export const getChannelSymbol = Symbol('getChannel');

export function GetChannel() {
  return function (target: any, methodName: string, parameterIndex: number) {
    const metadata = Reflect.getOwnMetadata(getChannelSymbol, target, methodName) || [];

    metadata.push(parameterIndex);
    Reflect.defineMetadata(getChannelSymbol, metadata, target, methodName);  
  }
}
