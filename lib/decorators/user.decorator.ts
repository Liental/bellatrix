export const getUserSymbol = Symbol('getUser');

export function GetUser() {
  return function (target: any, methodName: string, parameterIndex: number) {
    const metadata = Reflect.getOwnMetadata(getUserSymbol, target, methodName) || [];

    metadata.push(parameterIndex);
    Reflect.defineMetadata(getUserSymbol, metadata, target, methodName);  
  }
}
