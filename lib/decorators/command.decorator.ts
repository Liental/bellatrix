import { CommandModel } from '@cmd-handler/models/command.model';
import { getChannelSymbol } from './channel.decorator';
import { getMessageSymbol } from './message.decorator';
import { getUserSymbol } from './user.decorator';

export function Command(name: string, help: string) {
  return function (target: any, key: string, descriptor: PropertyDescriptor) {
    const command: CommandModel = { name, help, key, params: [] };

    if (target.commands) {
      target.commands.push(command);
    } else {
      target.commands = [ command ] ;
    }
    
    const previousValue = descriptor.value;
    const newValue = function (...args: any[]) {
      const argsLength = args.length;
      const funcLength = command.params.filter(param => !param.options?.optional).length;

      if (argsLength < funcLength) {
        const sliced = command.params.slice(argsLength);
        let message = `The required number of arguments is ${funcLength}, you provided ${argsLength}.\n\n`;

        message += 'The missing arguments are:\n';
        message += sliced.map(param => `- ${param.name}: ${param.help}`).join('\n');

        return message;
      }

      const getUserParameter = Reflect.getOwnMetadata(getUserSymbol, target, key) || [];
      const getMessageParameter = Reflect.getOwnMetadata(getMessageSymbol, target, key) || [];
      const getChannelParameter = Reflect.getOwnMetadata(getChannelSymbol, target, key) || [];

      for (const param of getUserParameter) {
        args[param] = this.message.author;
      }

      for (const param of getMessageParameter) {
        args[param] = this.message;
      }

      for (const param of getChannelParameter) {
        args[param] = this.message.channel;
      }

      return previousValue.bind(this)(...args);
    };

    target[descriptor.value.name] = newValue;
    descriptor.value = target[descriptor.value.name];
  }
}