import { DiceModule } from '@cmd-modules/dice/dice.module';
import { EmarModule } from '@cmd-modules/emar/emar.module';
import { EmoteModule } from '@cmd-modules/emote/emote.module';
import { InpostModule } from '@cmd-modules/inpost/inpost.module';
import { MagicModule } from '@cmd-modules/magic/magic.module';
import { PrefixModule } from '@cmd-modules/prefix/prefix.module';
import { ReminderModule } from '@cmd-modules/reminder/reminder.module';
import { Client, Message, MessageEmbed } from 'discord.js';
import { container, injectable } from 'tsyringe';
import { EmbedHelper } from './helpers/embed.helper';
import { PrefixRepo } from './repositories/prefix.repository';
import { ClientManager } from './services/client-manager.service';

@injectable()
export class BotModule {
  private pattern = /[^\s"']+|"([^"]*)"|'([^']*)'/gm;
  private clientManager: ClientManager;
  private prefixRepo: PrefixRepo;
  
  private modules = [
    container.resolve(InpostModule),
    container.resolve(ReminderModule),
    container.resolve(MagicModule),
    container.resolve(EmarModule),
    container.resolve(PrefixModule),
    container.resolve(DiceModule),

    // TODO finish that sucker
    // container.resolve(EmoteModule)
  ];

  constructor(private botClient: Client) {
    this.prefixRepo = container.resolve(PrefixRepo);
    this.clientManager = container.resolve(ClientManager);
    this.clientManager.client = botClient;
  }

  private generateHelpMessage(moduleText: string, messageSplit: string[]) {
    const commandText = messageSplit.shift();
    const moduleIndex = this.modules.findIndex(m => m.prefix === moduleText);

    if (moduleIndex !== -1) {
      const commandModule = this.modules[moduleIndex];
      return commandModule.generateHelpMessage(commandText);
    }

    const fields = [];

    for (const module of this.modules) {
      fields.push({ name: module.prefix, help: module.help });
    }

    return fields;
  }

  public async executeCommand(messageInstance: Message) {
    const command = messageInstance.content;
    const channelId = messageInstance.channel.id;
    const dbPrefix = await this.prefixRepo.getPrefix(channelId);
    const prefix = dbPrefix?.prefix || '!bell ';

    if (!command.startsWith(prefix)) {
      return;
    }

    const message = command.replace(prefix, '');
    const matchedParameters = message.match(this.pattern);

    const parameters = matchedParameters.map(parameter => {
      if (parameter.startsWith('"') && parameter.endsWith('"')) {
        parameter = parameter.substring(1, parameter.length - 1);
      }

      return parameter;
    });

    let moduleText = parameters.shift();
    let commandText = parameters.shift();
    let moduleIndex = this.modules.findIndex(m => m.prefix === moduleText);
    const embed = EmbedHelper.generateEmbed(`result of the ${commandText || 'help'} command`);
    
    if (moduleIndex !== -1 && commandText) {
      const commandModule = this.modules[moduleIndex];
      const commandResult = commandModule.executeCommand(messageInstance, this.clientManager?.client, commandText, parameters);
      const result = commandResult instanceof Promise ? await commandResult : commandResult;

      if (result instanceof MessageEmbed) {
        return result;
      } else {
        embed.addField(`command's result`, result || 'the command is not implemented yet');
      }
    
      return embed;
    }

    let fields = this.generateHelpMessage(moduleText, [ commandText ]);
    
    if (moduleText === 'help') {
      fields = this.generateHelpMessage(commandText, parameters);
    }

    for (const field of fields) {
      embed.addField(field.name, field.help);
    }

    return embed;
  }
}