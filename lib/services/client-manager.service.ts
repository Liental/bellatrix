import { Client } from 'discord.js';
import { Lifecycle, scoped } from 'tsyringe';

@scoped(Lifecycle.ContainerScoped)
export class ClientManager {
  public client: Client;
}
