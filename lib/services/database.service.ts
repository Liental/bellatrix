import { Connection, EntityManager, IDatabaseDriver, MikroORM } from 'mikro-orm';
import { Subject } from 'rxjs';
import { Lifecycle, scoped } from 'tsyringe';

@scoped(Lifecycle.ContainerScoped)
export class Database {
  private orm: MikroORM;
  private manager: EntityManager<IDatabaseDriver<Connection>>;

  public connection$ = new Subject();

  constructor() {
    MikroORM.init({
      type: 'sqlite',
      dbName: './database/bell-bot-db.sqlite',
      entities: [ './lib/**/*.entity.ts' ],
      entitiesTs: [ './lib/**/*.entity.ts' ]
    }).then(result => {
      this.orm = result;
      this.manager = this.orm.em;
      this.connection$.next(true);
    });
  }

  public getManager<T>(entity: new () => T) {
    return this.manager.getRepository(entity);
  }
}
