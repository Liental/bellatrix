export function setDecoratorValues (symbol: Symbol, target: any, methodName: string, value: any, bindTo?: any) {
  const parameters = Reflect.getOwnMetadata(symbol, target, methodName) || []
  const previous = target[methodName];

  target[methodName] = (...args: any[]) => {
    for (const param of parameters) {
      if (typeof value === 'function') {
        args[param] = value();
      } else {
        args[param] = value;
      }
    }

    return previous.bind(bindTo || target)(...args);
  }
}
