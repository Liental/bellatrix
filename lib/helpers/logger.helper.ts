import moment from 'moment';
import ansicolor from 'ansicolor';

export class Logger {
  public pattern = 'DD-MM-YYYY HH:mm:ss';
  private name: string;
  
  constructor(private _name?: string) {
    this.name = _name ? `[${_name}]` : '';
  }
  
  log(...args: string[]) {
    const date = moment().format(this.pattern);
    console.log(ansicolor.green(`[LOG][${date}]${this.name} ${args.join(' ')}`))
  }

  warn(...args: string[]) {
    const date = moment().format(this.pattern);
    console.warn(ansicolor.yellow(`[WARN][${date}]${this.name} ${args.join(' ')}`))
  }

  error(...args: string[]) {
    const date = moment().format(this.pattern);
    console.error(ansicolor.red(`[ERROR][${date}]${this.name} ${args.join(' ')}`))
  }
};