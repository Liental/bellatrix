import { MessageEmbed } from 'discord.js';

export class EmbedHelper {
  static generateEmbed(title?: string) {
    return new MessageEmbed()
    .setColor('#ff625e')
    .setAuthor(title || 'result', 'https://cdn.discordapp.com/avatars/748239870538940457/695e067cbaafbb5a48bd24bc1eb39264.png')
    .setTimestamp()
  }
}
