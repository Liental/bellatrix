import { EmbedHelper } from '@cmd-helpers/embed.helper';
import { Card } from 'scryfall-sdk';



export class MagicHelper {
  static replaceMana(text: string) {
    text = text.replace(/\{(\d|\w)\}/g, '[$1] ');
    return text.substr(0, text.length);
  }

  static generateEmbed(title?: string) {
    return EmbedHelper.generateEmbed(title)
      .setFooter('informations provided by ScryFall SDK', 'https://assets.scryfall.com/icon.png?v=259192bf8ddd3286cf6fd82bec8649f93bc4d42b64a8ab18e7764164e7cd6a7e');
  }

  static generateCardEmbed(card: Card) {
    const cardMana = MagicHelper.replaceMana(card.mana_cost || 'none')
    const description = MagicHelper.replaceMana(card.oracle_text || 'none');

    const colors = {
      'G': 'green',
      'W': 'white',
      'B': 'black',
      'U': 'blue',
      'R': 'red'
    }

    const color = card.color_identity.map(c => colors[c]).join(', ');

    return EmbedHelper.generateEmbed('card details')
      .setImage(card.image_uris.normal)
      .setFooter('informations provided by ScryFall SDK', 'https://assets.scryfall.com/icon.png?v=259192bf8ddd3286cf6fd82bec8649f93bc4d42b64a8ab18e7764164e7cd6a7e')
      .addField('name', card.name.toLowerCase(), true)
      .addField('type', (card.type_line || 'none').toLowerCase(), true)
      .addField('mana cost', cardMana, true)
      .addField('rarity', card.rarity, true)
      .addField('power', card.power || 'none', true)
      .addField('toughness', card.toughness || 'none', true)
      .addField('color', color || 'none', true)
      .addField('converted mana cost', card.cmc || 'none', true)
      .addField('released at', card.released_at || 'none', true)
      .addField('description', description)
      .addField('flavor text', card.flavor_text || 'none');
  }
}