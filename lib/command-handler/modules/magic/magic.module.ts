import axios from 'axios';
import { Card } from 'scryfall-sdk';
import { Command } from '@cmd-decorators/command.decorator';
import { Module } from '@cmd-decorators/module.decorator';
import { CommandHandler } from '@cmd-handler/command-handler';
import { MagicHelper } from './helpers/magic.helper';
import { Param } from '@cmd-decorators/param.decorator';

@Module('magic', 'get information about cards, boosters and more of the MTG stuff')
export class MagicModule extends CommandHandler {
  private readonly url = 'https://api.scryfall.com/';
  
  @Command('card-random', 'returns some informations about a random card')
  public async getRandomCard() {
    return axios.get<Card>(this.url + 'cards/random')
      .then(result => MagicHelper.generateCardEmbed(result.data))
      .catch(e => {
        this.logger.error(`Could not query the random card data (${e.message})`)
        return 'an error occurred while querying the information';
      });
  }

  @Param('name', 'the name of the card you want to see')
  @Command('card', 'returns some informations about a card matching given name')
  public async getCardByName(name: string) {
    return axios.get(this.url + '/cards/search?q=' + name)
      .then(result => MagicHelper.generateCardEmbed(result.data.data[0]))
      .catch(e => {
        this.logger.error(`Could not query the card data (${e.message})`)
        return 'an error occurred while querying the information';
      });
  }

  @Param('name', 'the name to autocomplete')
  @Command('card-autocomplete', 'returns autocompletion of a given card name')
  public async getCardAutocompletion(name: string) {
    return axios.get(this.url + '/cards/autocomplete?q=' + name)
      .then(result => MagicHelper.generateEmbed('autocomplete result').addField('matching cards', result.data.data))
      .catch(e => {
        this.logger.error(`Could not query the autocomplete data (${e.message})`)
        return 'an error occurred while querying the information';
      });
  }

}