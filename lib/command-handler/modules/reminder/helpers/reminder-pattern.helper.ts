export class ReminderPattern {
  public static minutePattern = /(\d+)m/gm;
  public static hourPattern = /(\d+)h/gm;
  public static dayPattern = /(\d+)d/gm;
  
  static parse(pattern: string) {
    pattern.match(this.minutePattern);
    pattern.match(this.hourPattern);
    pattern.match(this.dayPattern);
    
    const minutes = ReminderPattern.minutePattern.exec(pattern);
    const hours = ReminderPattern.hourPattern.exec(pattern);
    const days = ReminderPattern.dayPattern.exec(pattern);

    const minutesInt = minutes ? 1000 * 60 * parseInt(minutes[1]) : 0;
    const hoursInt = hours ? 1000 * 60 * 60 * parseInt(hours[1]) : 0;
    const daysInt = days ? 1000 * 60 * 60 * 24 * parseInt(days[1]) : 0;

    return minutesInt + hoursInt + daysInt;
  }
  
  static validate(pattern: string) {
    return ReminderPattern.parse(pattern) > 0;
  }

  static from(time: number) {
    if (time < 0 || !time) {
      time = 0;
    }

    const days = Math.floor(time / 86400000);
    const hours = Math.floor((time % 86400000) / 3600000);
    const minutes = Math.floor((time % 3600000) / 60000);

    return `${days} days, ${hours} hours, ${minutes} minutes (${days}d${hours}h${minutes}m)`;
  }

  static compare(pattern: string, date: Date) {
    return date.getTime() % ReminderPattern.parse(pattern);
  }
}
