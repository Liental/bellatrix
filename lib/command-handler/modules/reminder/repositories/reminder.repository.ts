import { Repo } from '@cmd-decorators/repo.decorator';
import { BaseRepo } from 'lib/repositories/base.repository';
import { Reminder } from '../entities/reminder.entity';

@Repo(Reminder)
export class ReminderRepo extends BaseRepo<Reminder> {
  public saveReminder(reminder: Reminder) {
    return this.repo.persistAndFlush(reminder)
  }

  public updateReminder(reminder: Reminder) {
    return this.repo.nativeUpdate({ id: reminder.id }, reminder);
  }

  public findById(id: number) {
    return this.repo.findOne({ id });
  }

  public findByUserId(id: number, userId: string) {
    return this.repo.findOne({ id, userId });
  }

  public findAllByUserId(userId: string) {
    return this.repo.find({ userId, deleted: false });
  }

  public removeById(id: number) {
    return this.repo.nativeUpdate({ id }, { deleted: true });
  }

  public removeByUserId(id: number, userId: string) {
    return this.repo.nativeUpdate({ id, userId }, { deleted: true });
  }

  public findBeforeDate(date: Date): Promise<Reminder[]> {
    return this.createQueryBuilder()
      .where('reminder_time <= ?', [ date.getTime() ])
      .andWhere('deleted = false')
      .execute();
  }
}
