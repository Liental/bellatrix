import { Command } from '@cmd-decorators/command.decorator';
import { Module } from '@cmd-decorators/module.decorator';
import { Param } from '@cmd-decorators/param.decorator';
import { CommandHandler } from '@cmd-handler/command-handler';
import { Reminder } from './entities/reminder.entity';
import { ReminderRepo } from './repositories/reminder.repository';
import { GetUser } from '@cmd-decorators/user.decorator';
import { Client, User } from 'discord.js';
import { Cron } from '@cmd-decorators/cron.decorator';
import { GetClient } from '@cmd-decorators/client.decorator';
import { EmbedHelper } from '@cmd-helpers/embed.helper';
import { ReminderPattern } from './helpers/reminder-pattern.helper';
import { OptionalParam } from '@cmd-decorators/optional-param.decorator';

@Module('reminder', 'reminds you of whatever the heck you want')
export class ReminderModule extends CommandHandler {  
  constructor(private repo: ReminderRepo) {
    super();
  }
  
  @Cron('* * * * *')
  private async checkNotifications(@GetClient() client: Client) {
    const date = new Date();
    const currentReminders = await this.repo.findBeforeDate(date);

    for (const reminder of currentReminders) {
      const user = await client.users.fetch(reminder.userId);
      const channel = await user.createDM();
      const parsed = ReminderPattern.parse(reminder.repeatEvery || '');

      if (reminder.repeatEvery) {
        reminder.reminderTime = new Date(date.getTime() + parsed);
        await this.repo.updateReminder(reminder);
      }

      const nextRemind = ReminderPattern.from(parsed);
      const message = EmbedHelper.generateEmbed('a friendly reminder')
        .addField('reminder content', reminder.content)
        .addField('next notification in', reminder.repeatEvery ? nextRemind : 'never');

      channel.send(message)
        .catch(e => this.logger.error(`Could not send the reminder message (${e.message})`))
        .then(r => !reminder.repeatEvery && this.repo.removeById(reminder.id));
    }
  }
  
  @Param('content', 'the content of the reminder')
  @Param('remind in', 'in how much time do you want to be reminded (\\*d\\*h\\*m)')
  @OptionalParam('repeat every', 'definition of how often should the reminder be repeated (\\*d\\*h\\*m)')
  @Command('add', 'adds a new reminder and associates it with the user')
  public async addReminder(content: string, remindIn: string, pattern: string, @GetUser() user: User) {
    const reminder = new Reminder();
    const time = ReminderPattern.parse(remindIn)
    const date = new Date(new Date().getTime() + time);
    
    reminder.reminderTime = date;
    reminder.content = content;
    reminder.userId = user.id;

    if (pattern && ReminderPattern.validate(pattern)) {
      reminder.repeatEvery = pattern;
    } else if (pattern) {
      return 'provided pattern is invalid';
    }

    return this.repo.saveReminder(reminder)
      .then(() => `successfuly created a reminder!`)
      .catch(e => {
        this.logger.error(e.message);
        return 'an error occurred while creating a reminder';
      });
  }

  @Param('id', 'the id of the reminder to edit')
  @Param('content', 'the content of the reminder')
  @OptionalParam('remind in', 'in how much time do you want to be reminded (\\*d\\*h\\*m)')
  @OptionalParam('repeat every', 'definition of how often should the reminder be repeated (\\*d\\*h\\*m)')
  @Command('edit', 'edits the desired reminder')
  public async editReminder(id: number, content: string, remindIn: string, pattern: string, @GetUser() user: User) {
    const reminder = await this.repo.findByUserId(id, user.id);
    
    if (!reminder || reminder.deleted) {
      return 'could not find a reminder with provided ID';
    }

    if (remindIn) {
      const time = ReminderPattern.parse(remindIn)
      const date = new Date(new Date().getTime() + time);
      
      reminder.reminderTime = date;
    }

    if (pattern && ReminderPattern.validate(pattern)) {
      reminder.repeatEvery = pattern;
    } else if (pattern) {
      return 'provided pattern is invalid';
    }

    reminder.content = content;

    return this.repo.updateReminder(reminder)
      .then(() => 'successfuly edited the reminder!')
      .catch(e => {
        this.logger.error(e.message);
        return 'an error occurred while creating a reminder';
      });
  }

  @Param('id', 'the id of the reminder')
  @Command('get', 'get the details of a reminder')
  public async getReminder(id: number, @GetUser() user: User) {      
      return this.repo.findByUserId(id, user.id)
      .then(reminder => {
        if (reminder === null) {
          return 'could not find a reminder with provided ID';
        }
        
        const nextRemind = ReminderPattern.from(reminder.reminderTime.getTime() - new Date().getTime());
        return EmbedHelper.generateEmbed()
          .addField('reminder content', reminder.content)
          .addField('remind in', nextRemind)
          .addField('deleted', reminder.deleted ? 'true' : 'false', true);
      });
  }

  @Command('list', `list user's active reminders`)
  public async listReminders(@GetUser() user: User) {
    return this.repo.findAllByUserId(user.id)
      .then(result => {
        if (!result || !result.length) {
          return 'you have no active reminders';
        }
        
        return EmbedHelper.generateEmbed()
          .addField('active reminders', result.map(reminder => `- ${reminder.id}: ${reminder.content}`));
      });
  }

  @Param('id', 'the id of the reminder')
  @Command('remove', 'removes a reminder by given id')
  public async removeReminder(id: number, @GetUser() user: User) {
    return this.repo.removeByUserId(id, user.id)
      .then(r => r ? 'successfully removed the reminder' : 'could not find a reminder with provided ID')
      .catch(e => 'an error occurred while removing the reminder');
  }
}
