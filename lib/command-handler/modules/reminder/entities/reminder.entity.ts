import { Entity, PrimaryKey, Property } from 'mikro-orm';

@Entity()
export class Reminder {
  @PrimaryKey()
  id: number;

  @Property()
  content!: string;

  @Property()
  userId!: string;

  @Property({ nullable: true })
  repeatEvery!: string;

  @Property()
  reminderTime!: Date;

  @Property({ onUpdate: () => new Date() })
  updatedAt = new Date();
  
  @Property()
  createdAt = new Date();

  @Property({ default: false })
  deleted: boolean;
}