import { Module } from '@cmd-decorators/module.decorator';
import { Param } from '@cmd-decorators/param.decorator';
import { Command } from '@cmd-decorators/command.decorator';
import { CommandHandler } from '@cmd-handler/command-handler';
import { InpostStatus } from './models/inpost-status.model';
import { InpostHelper } from './helpers/inpost.helper';
import { Cron } from '@cmd-decorators/cron.decorator';
import { Client, User } from 'discord.js';
import { GetClient } from '@cmd-decorators/client.decorator';
import { GetUser } from '@cmd-decorators/user.decorator';
import { EmbedHelper } from '@cmd-helpers/embed.helper';
import { InpostRepo } from './repositories/inpost.repository';

@Module('inpost', 'inpost delivery shipping information module')
export class InpostModule extends CommandHandler {
  private helper = new InpostHelper();

  constructor(private repo: InpostRepo) {
    super();
  }

  @Cron('5 * * * * *')
  private async checkRegisteredStatus(@GetClient() client: Client) {
    const listeners = await this.repo.findAll();
    
    for (const listener of listeners) {
      const result = await this.helper.fetchCurrentStatus(listener.trackingId);
      const isDelivered = result.status === 'delivered';
      
      if (isDelivered) {
        this.repo.remove(listener) 
          .catch(e => this.logger.error(`Failed to remove an Inpost tracker (${e})`));
      }
      
      if (listener.status !== result.status) {
        if (listener.status) {
          const embed = this.helper.generateEmbed(listener.trackingId, 'new tracking status');
          const user = await client.users.fetch(listener.userId);
          const channel = await user.createDM();
  
          embed.addField('current tracking status', result.history);        
          channel.send(embed);  
        }
        
        if (!isDelivered) {
          listener.status = result.status;

          this.repo.update(listener)
            .catch(e => this.logger.error(`Failed to update an Inpost tracker (${e})`));
        }  
      }
    }
  } 

  @Param('id', 'the shipping id to check')
  @Command('status', 'get the status of provided package')
  public async getStatus(id: string) {
    return this.helper.fetchCurrentStatus(id)
      .then(result => result.history)
      .catch(() => 'could not connect with inpost servers');
  }

  @Param('id', 'the shipping id to track')
  @Command('add', 'add a listener to track the package')
  public async addListener(trackingId: string, @GetUser() user: User) {    
    return this.repo.addPackageListener(user.id, trackingId)
      .then(() => 'successfully added the package to tracking list')
      .catch(e => e || 'an error occurred while adding the tracker');
  }

  @Param('id', 'the shipping id to stop stracking')
  @Command('remove', 'remove a tracker')
  public async removeListener(trackingId: string, @GetUser() user: User) {    
    return this.repo.removeByTrackingId(user.id, trackingId)
      .then(() => 'successfully removed a tracker')
      .catch(e => e || 'an error occurred while removing the tracker');
  }
};