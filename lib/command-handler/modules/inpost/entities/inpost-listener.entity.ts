import { Entity, PrimaryKey, Property } from 'mikro-orm';

@Entity()
export class InpostListener {
  @PrimaryKey()
  id: number;

  @Property()
  userId!: string;

  @Property()
  trackingId!: string;

  @Property({ nullable: true })
  status: string;
}
