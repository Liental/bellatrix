import { EmbedHelper } from '@cmd-helpers/embed.helper';
import axios from 'axios';
import moment from 'moment';
import { InpostResult } from '../models/inpost-result.model';
import { InpostStatus } from '../models/inpost-status.model';

export class InpostHelper {
  private readonly url = 'https://inpost.pl/shipx-proxy/?number=';
  private readonly publicUrl = 'https://inpost.pl/en/find-parcel?number=';
  private statuses: InpostStatus[] = [];
  
  constructor() {
    axios
      .get('https://api-shipx-pl.easypack24.net/v1/statuses')
      .then(result => this.statuses = result.data.items);
  }

  public generateEmbed(trackingId: string, title?: string) {
    return EmbedHelper.generateEmbed(title)
      .setURL('https://inpost.pl/en/find-parcel?number=' + trackingId);
  }

  public async fetchCurrentStatus(id: string) {
    const headers = { 'X-Requested-With': 'XMLHttpRequest' };

    return axios.get(this.url + id, { headers })
      .then(result => {
        const inpostResult = new InpostResult();
        const details = result.data.tracking_details;
        
        inpostResult.status = result.data.status;
        inpostResult.history = details.map((segment: any, i: number) => {
          const index = this.statuses.findIndex(status => status.name === segment.status);
          const fix = i ? '' : '**';
          const date = moment(segment.datetime).format('DD-MM-YYYY HH:mm');
          
          return `${fix}${date}: ${this.statuses[index].title}${fix}`;
        });

        return inpostResult;
    });
  }
}