import { Repo } from '@cmd-decorators/repo.decorator';
import { BaseRepo } from 'lib/repositories/base.repository';
import { InpostListener } from '../entities/inpost-listener.entity';

@Repo(InpostListener)
export class InpostRepo extends BaseRepo<InpostListener> {
  public async addPackageListener(userId: string, trackingId: string) {
    const listener = await this.repo.findOne({ userId, trackingId });

    if (!listener) {
      const newListener = new InpostListener();

      newListener.userId = userId;
      newListener.trackingId = trackingId;

      return this.repo.persistAndFlush(newListener);
    }
    
    throw 'an active listener for this tracking id already exists';
  }

  public update(listener: InpostListener) {
    return this.repo.nativeUpdate({ id: listener.id }, listener);
  } 

  public remove(listener: InpostListener) {
    return this.repo.removeAndFlush(listener);
  }

  public removeByTrackingId(userId: string, trackingId: string) {
    return this.repo.nativeDelete({ userId, trackingId });
  }

  public findAll() {
    return this.repo.findAll();
  }
}