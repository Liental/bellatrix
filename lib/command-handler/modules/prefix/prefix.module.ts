import { GetChannel } from '@cmd-decorators/channel.decorator';
import { Command } from '@cmd-decorators/command.decorator';
import { Module } from '@cmd-decorators/module.decorator';
import { Param } from '@cmd-decorators/param.decorator';
import { CommandHandler } from '@cmd-handler/command-handler';
import { Channel } from 'discord.js';
import { PrefixRepo } from 'lib/repositories/prefix.repository';

@Module('prefix', 'manage the prefix for current channel')
export class PrefixModule extends CommandHandler {
  constructor(private repo: PrefixRepo) {
    super();
  }
  
  @Param('prefix', 'the prefix you want to set for this channel')
  @Command('set', 'set the prefix for current channel')
  public async setPrefix(prefix: string, @GetChannel() channel: Channel) {
    if (!prefix.length) {
      return 'the prefix has to be longer than 0 characters';
    }
    
    return this.repo.setPrefix(channel.id, prefix.trim())
      .then(r => `successfully changed the prefix to ${prefix}`)
      .catch(e => {
        this.logger.error(e.message);
        return 'an error occurred while changing the prefix';
      });
  }

  @Command('get', 'get the prefix of the current channel')
  public async getPrefix(@GetChannel() channel: Channel) {
    return this.repo.getPrefix(channel.id)
      .then(r => `the current prefix of this channel is \`${r ? r.prefix : '!bell '}\``)
      .catch(e => {
        this.logger.error(e.message);
        return 'an error occurred while receiving the prefix';
      });
  }
}
