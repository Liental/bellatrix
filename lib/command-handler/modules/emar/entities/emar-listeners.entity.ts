import { Entity, PrimaryKey, Property } from 'mikro-orm';

@Entity()
export class EmarListeners {
  @PrimaryKey()
  id: number;

  @Property()
  userId!: string;
  
  @Property({ default: false })
  modeling: boolean;

  @Property({ default: false })
  cards: boolean;
}