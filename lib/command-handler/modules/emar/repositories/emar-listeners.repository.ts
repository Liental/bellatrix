import { Repo } from '@cmd-decorators/repo.decorator';
import { BaseRepo } from 'lib/repositories/base.repository';
import { EmarListeners } from '../entities/emar-listeners.entity';

@Repo(EmarListeners)
export class EmarListenersRepo extends BaseRepo<EmarListeners> {
  public async setCardListener(userId: string, enable: boolean) {
    const listener = await this.repo.findOne({ userId }) || new EmarListeners();
    
    listener.userId = userId;
    listener.cards = enable;

    return this.repo.persistAndFlush(listener);
  }

  public async setModelingListener(userId: string, enable: boolean) {
    const listener = await this.repo.findOne({ userId }) || new EmarListeners();
    
    listener.userId = userId;
    listener.modeling = enable;

    return this.repo.persistAndFlush(listener);
  }

  public getCardListeners() {
    return this.repo.find({ cards: true });
  }
  
  public getModelingListeners() {
    return this.repo.find({ modeling: true });
  }  
}