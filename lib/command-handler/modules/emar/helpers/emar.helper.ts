import { EmbedHelper } from '@cmd-helpers/embed.helper';
import axios from 'axios';
import cheerio from 'cheerio';
import { ClientManager } from 'lib/services/client-manager.service';
import { container } from 'tsyringe';
import { EmarListeners } from '../entities/emar-listeners.entity';

export class EmarHelper {
  public static async getCardHeaders() {
    return axios.get('http://emar.olsztyn.pl/info/gry')
      .then(result => {
        const parser = cheerio.load(result.data);
        const headers = parser('.categoryhead a');
        return headers.toArray().map(header => parser(header).text());
      });
  }

  public static async getModelingHeaders() {
    return axios.get('http://emar.olsztyn.pl/info/modelarstwo')
      .then(result => {
        const parser = cheerio.load(result.data);
        const headers = parser('.categoryhead a');
        return headers.toArray().map(header => parser(header).text());
      });
  }

  public static async spreadTheNews(titles: string[], previousTitles: string[], listeners: EmarListeners[]) {
    const clientManager = container.resolve(ClientManager);
    const client = clientManager.client;

    const newTitles = titles.filter(title => !previousTitles.includes(title));

    if (previousTitles.length && newTitles.length) {
      const embed = EmbedHelper.generateEmbed('new emar news detected')
        .addField('new posts', newTitles);
      
      for (const listener of listeners) {
        const user = await client.users.fetch(listener.userId);
        const channel = await user.createDM();

        channel.send(embed);
      }
    }

    return previousTitles.concat(newTitles);
  }
}