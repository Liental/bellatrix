import { Client, User } from 'discord.js';
import { Cron } from '@cmd-decorators/cron.decorator';
import { Module } from '@cmd-decorators/module.decorator';
import { CommandHandler } from '@cmd-handler/command-handler';
import { EmbedHelper } from '@cmd-helpers/embed.helper';
import { GetClient } from '@cmd-decorators/client.decorator';
import { EmarListenersRepo } from './repositories/emar-listeners.repository';
import { GetUser } from '@cmd-decorators/user.decorator';
import { Command } from '@cmd-decorators/command.decorator';
import { EmarHelper } from './helpers/emar.helper';

@Module('emar', 'get current news from the emar store')
export class EmarModule extends CommandHandler {
  private previousCardTitles: string[] = [];
  private previousModelingTitles: string[] = [];
  
  constructor(private repo: EmarListenersRepo) {
    super();
  }
  
  @Cron('*/5 * * * *')
  private async checkCardNews() {
    const headerTitles = await EmarHelper.getCardHeaders();
    const listeners = await this.repo.getCardListeners();

    this.previousCardTitles = await EmarHelper.spreadTheNews(headerTitles, this.previousCardTitles, listeners);
  }

  @Cron('*/5 * * * *')
  private async checkModelingNews() {
    const headerTitles = await EmarHelper.getModelingHeaders();
    const listeners = await this.repo.getModelingListeners();

    this.previousModelingTitles = await EmarHelper.spreadTheNews(headerTitles, this.previousModelingTitles, listeners);
  }

  @Command('model', 'check current modeling news')
  public async getCurrentModeling() {
    const headerTitles = await EmarHelper.getModelingHeaders()
      .catch(e => 'an error occured while receiving the news');
    
    return headerTitles;
  }

  @Command('model-enable', 'adds the user to the emar modeling news listeners')
  public async addModelingListener(@GetUser() user: User) {
    return this.repo.setModelingListener(user.id, true)
      .then(r => 'successfully added the user to the listeners')
      .catch(e => {
        this.logger.error(`Could not add the user to listeners (${e.message})`)
        return 'an error occurred while adding the listener';
      });
  }

  @Command('model-disable', 'removes the user from the emar modeling news listeners')
  public async removeModelingListener(@GetUser() user: User) {
    return this.repo.setModelingListener(user.id, false)
      .then(r => 'successfully removed the user from the listeners')
      .catch(e => {
        this.logger.error(`Could not remove the user to listeners (${e.message})`)
        return 'an error occurred while removing the listener';
      });
  }

  @Command('cards', 'check current cards news')
  public async getCurrentCards() {
    const headerTitles = await EmarHelper.getCardHeaders()
      .catch(e => 'an error occured while receiving the news');
    
    return headerTitles;
  }

  @Command('cards-enable', 'adds the user to the emar card news listeners')
  public async addCardListener(@GetUser() user: User) {
    return this.repo.setCardListener(user.id, true)
      .then(r => 'successfully added the user to the listeners')
      .catch(e => {
        this.logger.error(`Could not add the user to listeners (${e.message})`)
        return 'an error occurred while adding the listener';
      });
  }

  @Command('cards-disable', 'removes the user from the emar card news listeners')
  public async removeCardListener(@GetUser() user: User) {
    return this.repo.setCardListener(user.id, false)
      .then(r => 'successfully removed the user from the listeners')
      .catch(e => {
        this.logger.error(`Could not remove the user to listeners (${e.message})`)
        return 'an error occurred while removing the listener';
      });
  }
}
