import { Repo } from '@cmd-decorators/repo.decorator';
import { BaseRepo } from 'lib/repositories/base.repository';
import { Emote } from '../entities/emote.entity';

@Repo(Emote)
export class EmoteRepository extends BaseRepo<Emote> {
  public async addQueue(name: string, emotes: string[]) {
    const emote = Object.assign(new Emote(), { name, emotes });
    return this.repo.create(emote);
  }
}
