import { Entity, PrimaryKey, Property } from 'mikro-orm';

@Entity()
export class Emote {
  @PrimaryKey()
  id: number;

  @Property()
  name: string;

  @Property({ type: Array })
  emotes: string[];
}
