import { GetChannel } from '@cmd-decorators/channel.decorator';
import { Command } from '@cmd-decorators/command.decorator';
import { Module } from '@cmd-decorators/module.decorator';
import { Param } from '@cmd-decorators/param.decorator';
import { CommandHandler } from '@cmd-handler/command-handler';
import { Channel } from 'discord.js';
import { EmoteRepository } from './repositories/emote.repository';

@Module('emote', 'create a queue of emotes to animate')
export class EmoteModule extends CommandHandler {
  constructor(private repo: EmoteRepository) {
    super();
  }
  
  @Param('name', 'the name of the animation')
  @Param('list', 'list of emotes to loop through')
  @Command('add', 'adds a loop of emotes')
  public async add(@GetChannel() channel: Channel, name: string, ...emotes: string[]) {
    name = arguments[1];
    emotes = Array.from(arguments).slice(1);

    return this.repo.addQueue(name, emotes)
      .then(r => { console.log(r) })
      .catch(e => {});
  }
}