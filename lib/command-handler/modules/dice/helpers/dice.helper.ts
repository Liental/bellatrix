export class DiceHelper {
  static roll(sides: number, minimal: number = 1) {
    return Math.floor(Math.random() * (sides - minimal)) + minimal;
  }

  static avg(sides: number, amount: number = 1) {
    return Math.round((sides + 1) / 2) * amount;
  } 
}