import { Command } from '@cmd-decorators/command.decorator';
import { Module } from '@cmd-decorators/module.decorator';
import { OptionalParam } from '@cmd-decorators/optional-param.decorator';
import { Param } from '@cmd-decorators/param.decorator';
import { CommandHandler } from '@cmd-handler/command-handler';
import { DiceHelper } from './helpers/dice.helper';

@Module('dice', 'roll some dices')
export class DiceModule extends CommandHandler {
  private readonly pattern = /(?<times>\d*)?d(?<sides>\d*)r?(?<minimal>\d*)?(?<adds>\+\d*)*/gm
  
  @Param('schema', 'schema of the dice roll [number_of_rolls]d<sides_of_dice>r[minimal_value]')
  @OptionalParam('name', 'name of the skill roll')
  @Command('roll', 'roll a dice')
  public roll(content: string, name: string) {
    const regex = new RegExp(this.pattern);
    const match = regex.exec(content);
    const groups = match?.groups;

    if (!groups) {
      return 'something went wrong';
    }
    
    const times = Number(groups.times) || 1;    
    const minimal = Number(groups.minimal) || 1;
    const sides = Number(groups.sides) + 1;

    if (!groups.sides) {
      return 'you need to provide sides of the dice';
    }

    const numbers = new Array();
    let stringResult = new String('(');

    for (let i = 0; i < times; i++) {
      const rand = DiceHelper.roll(sides, minimal);
      numbers.push(rand);

      stringResult += `${i > 0 ? ' + ' : ''}${rand}`;
    }

    stringResult += ')'

    const addPattern = /(?<positive>\+|\-)(?<number>\d*)/gm;
    let current: RegExpExecArray;

    while (current = addPattern.exec(content)) {
      const positive = current.groups.positive === '+';
      const product = Number(current.groups.number) * (positive ? 1 : -1);
      
      numbers.push(product)
      stringResult += ` ${current.groups.positive} ${Math.abs(product)}`;
    }

    const sum = numbers.reduce((prev, curr) => prev + curr);
    const avg = DiceHelper.avg(sides, times);
    
    return `\
    average: ${avg}
    ${name || 'dice roll'}: **${content}**\n ${stringResult} = **${sum}**`;
  }
}