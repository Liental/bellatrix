import { CronJob } from 'cron';
import { CommandParam } from './command-param.model';

export class CommandModel {
  name: string;
  help: string;
  key: string;
  params: CommandParam[];
}
