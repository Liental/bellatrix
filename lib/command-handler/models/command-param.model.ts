import { ParamOptions } from './param-options.model';

export class CommandParam {
  name: string;
  help: string;
  options: ParamOptions;
}