import { CronJob } from 'cron';
import { Client, Message } from 'discord.js';
import { Logger } from '@cmd-helpers/logger.helper';
import { CommandModel } from './models/command.model';
import { CronMethods } from './models/cron-method.model';

export class CommandHandler {
  public readonly prefix: string;
  public readonly help: string;
  public readonly commands: CommandModel[];
  public readonly cronMethods: CronMethods[];
  private readonly cronJobs: CronJob[]; 

  protected logger: Logger; 

  constructor () {
    this.logger = new Logger(this.constructor.name);
    this.logger.log(`Registered module`);
  }

  public generateHelpMessage(name: string) {
    const index = this.commands.findIndex(c => c.name === name);
    const fields = [];

    if (index !== -1) {
      const command = this.commands[index];
      fields.push({ name: command.name, help: command.help });

      for (const param of command.params) {
        const optional = param.options.optional ? `(optional)` : '';
        fields.push({ name: `${param.name} ${optional}`, help: param.help });
      }
    } else {
      for (const command of this.commands) {
        fields.push({ name: `${command.name}`, help: command.help });
      }
    }

    return fields;
  }

  public executeCommand(message: Message, client: Client, name: string, params: string[]) {
    const index = this.commands.findIndex(c => c.name === name);
    
    if (index !== -1) {
      const command = this.commands[index];
      return this[command.key].apply(Object.assign(this, { message, client }), params);
    }

    return `Provided command does not exist (${name})`;
  }
}