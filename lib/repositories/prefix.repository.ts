import { Repo } from '@cmd-decorators/repo.decorator';
import { Prefix } from 'lib/entities/prefix.entity';
import { BaseRepo } from 'lib/repositories/base.repository';

@Repo(Prefix)
export class PrefixRepo extends BaseRepo<Prefix> {
  public getPrefix(channelId: string) {
    return this.repo.findOne({ channelId });
  }

  public async setPrefix(channelId: string, prefix: string) {
    const databasePrefix = await this.repo.findOne({ channelId }) || new Prefix();

    databasePrefix.prefix = prefix;
    databasePrefix.channelId = channelId;

    return this.repo.persistAndFlush(databasePrefix);
  }
}
