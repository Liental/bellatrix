import { QueryBuilder } from '@mikro-orm/sqlite';
import { EntityRepository } from 'mikro-orm';

export class BaseRepo<T> {
  protected repo: EntityRepository<T>;

  protected createQueryBuilder(): QueryBuilder {
    return this.repo['createQueryBuilder']();
  }
}