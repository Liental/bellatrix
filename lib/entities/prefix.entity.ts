import { Entity, PrimaryKey, Property } from 'mikro-orm';

@Entity()
export class Prefix {
  @PrimaryKey()
  id: number;

  @Property()
  channelId: string;
  
  @Property({ default: '!bell' })
  prefix: string;
}
