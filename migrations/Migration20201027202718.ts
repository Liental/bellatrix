import { Migration } from '@mikro-orm/migrations';

export class Migration20201027202718 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table `prefix` (`id` integer not null primary key autoincrement, `channel_id` varchar not null, `prefix` varchar not null default \'!bell\');');
  }

}
