import { Migration } from '@mikro-orm/migrations';

export class Migration20201229220823 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table `emote` (`id` integer not null primary key autoincrement, `name` varchar not null, `emotes` text not null);');
  }

}
