import { Migration } from '@mikro-orm/migrations';

export class Migration20201025194456 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table `emar_listeners` (`id` integer not null primary key autoincrement, `user_id` varchar not null, `modeling` integer not null default false, `cards` integer not null default false);');
  }

}
