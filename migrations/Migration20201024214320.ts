import { Migration } from '@mikro-orm/migrations';

export class Migration20201024214320 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table `reminder` (`id` integer not null primary key autoincrement, `content` varchar not null, `user_id` varchar not null, `repeat_every` varchar null, `reminder_time` datetime not null, `updated_at` text not null, `created_at` text not null, `deleted` integer not null default false);');
  }

}
