import { Migration } from '@mikro-orm/migrations';

export class Migration20201104202650 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table `inpost_listener` (`id` integer not null primary key autoincrement, `user_id` varchar not null, `tracking_id` varchar not null, `status` varchar);');
  }

}
