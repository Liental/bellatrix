export default {
  type: 'sqlite',
  dbName: './database/bell-bot-db.sqlite',
  entities: [ './dist/**/*.entity.js' ],
  entitiesTs: [ './lib/**/*.entity.ts' ]
}
