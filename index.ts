import 'reflect-metadata';
import Discord from 'discord.js';
import { Logger } from '@cmd-helpers/logger.helper';
import { BotModule } from './lib/bot.module';

const logger = new Logger(); 
const bot = new Discord.Client();
const botCommands = new BotModule(bot);
const token = process.env.BELL_BOT_TOKEN;

bot.login(token)
  .then(() => logger.log('Successfully logged in'))
  .catch(error => logger.error(`Failed to log in (${error.message})`));
  
bot.on('message', async message => {
  const result = await botCommands.executeCommand(message);

  if (!result) {
    return;
  }

  message.channel.send(result)
    .catch(e => logger.error(`Could not send a message (${e})`));
});